﻿using System.Globalization;
using GenericMethods.Interfaces;

namespace Adapters
{
    public class GetIEEE754FormatAdapter : ITransformer<double, string>
    {
        public string Transform(double obj)
        {
            long v = BitConverter.DoubleToInt64Bits(obj);
            string binary = Convert.ToString(v, 2);
            return binary.PadLeft(64, '0');
        }
    }
}
