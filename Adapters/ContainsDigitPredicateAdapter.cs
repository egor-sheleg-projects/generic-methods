﻿using ContainsDigitPredicate;
using GenericMethods.Interfaces;

namespace Adapters
{
    public class ContainsDigitPredicateAdapter : IPredicate<int>
    {
        private readonly ContainsDigitValidator? validator;

        public ContainsDigitPredicateAdapter(ContainsDigitValidator? validator)
        {
            this.validator = validator;
        }

        public bool Verify(int obj)
        {
            if (this.validator is null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            return this.validator.Verify(obj);
        }
    }
}
