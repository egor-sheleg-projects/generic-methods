﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;

namespace Comparators
{
    public class IntegerByAbsComparer : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            if (Math.Abs(x) < Math.Abs(y))
            {
                return -1;
            }
            else if (Math.Abs(x) > Math.Abs(y))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
