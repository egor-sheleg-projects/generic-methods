﻿using System.Globalization;

namespace Comparators
{
    public class StringByLengthComparer : IComparer<string>
    {
        public int Compare(string? x, string? y)
        {
            if (string.IsNullOrEmpty(x) && string.IsNullOrEmpty(y))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(y))
            {
                return 1;
            }
            else if (string.IsNullOrEmpty(x))
            {
                return -1;
            }
            else
            {
                if (x.Length < y.Length)
                {
                    return -1;
                }
                else if (x.Length > y.Length)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
