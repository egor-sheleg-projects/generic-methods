﻿using System.Globalization;

namespace ContainsDigitPredicate
{
    public class ContainsDigitValidator
    {
        public int Digit { get; set; }

        public bool Verify(int value)
        {
            string dig = this.Digit.ToString(CultureInfo.InvariantCulture);
            string s = value.ToString(CultureInfo.InvariantCulture);

            foreach (char c in s)
            {
                if (c == dig[0])
                {
                    return true;
                }
            }

            return false;
        }
    }
}
